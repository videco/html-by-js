import Vue from 'vue'
import h from 'hyperscript'
import hh from 'hyperscript-helpers'

const {div, h1, h2, p, span, b, input, label} = hh(h)

const input_o_table = placeholder => 
  label('Aburrido repetir', [input({type:'text', placeholder})])

const madeWith = h1('.title', 'Hecho con Hyperscript')

document.getElementById('pre-container').appendChild(madeWith)

const dom2 = div([
    h2({attrs: {'v-on:click': 'sayHi'}}, 'Hello World!'),
    p('yeah', [span(' hommie'), b("!")]),
    input_o_table('pero así no está tan mal')
  ])

 new Vue({
  el: '#container',
  template: dom2.outerHTML,
  methods: {
    sayHi() {
      console.log("dom", dom2.outerHTML);
    }
  }
 })
